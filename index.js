console.log("Hello World");

let firstName = "John";
console.log("First Name: " + firstName);

let lastName = "Smith";
console.log("Last Name: " + lastName);

let age = 30;
console.log("Age: " + age)

let hobbies = ["Biking", "Mountain Climbing", "Swimming"]
console.log("Hobbies: ")

let city = "Lincoln"

let houseNumber = 32

let state = "Nebraska"

let street = "Washington"

function printName(name){
	console.log("My name is " + name);
}

function printName(firstName, lastName, age, hobbies){
	let details = {
		firstName,
		lastName,
		age,
		hobbies
	}
	console.log(details)
}

printName(firstName, lastName, age, hobbies)


let workAddress = {
	city,
	houseNumber,
	state,
	street
}


console.log("Work Address:", workAddress)

function personDetails(firstName, lastName, age) {
	return firstName + " " + lastName + " is " + age + " years of age."
}

let personInfo = personDetails(firstName, lastName, age)

console.log(personInfo)

function printHobbies(hobbies) {
	console.log("This was printed inside of the function", hobbies)
}

printHobbies(hobbies)

let isMarried = true;

console.log("The value of isMarried is: " + isMarried)